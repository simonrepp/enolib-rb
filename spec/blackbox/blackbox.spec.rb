# frozen_string_literal: true

input = <<~DOC.strip
> Text of Comment

Date: 1st of November in the year 2017

-- Book Description
Lacks a certain ...
something
-- Book Description

Shopping List:
- Apples
- Oranges

Telephone Numbers:
Ben = +49 1943 24724784
Rachel = +59 3459 35935593
`Dave` = +69 9025 93593531

# Body
## Limbs
### Left Arm

`--format pretty`: Pretty formatting option

`> Friends`:
- Jack the Ripper
- Frankenstein

``Use `${arg}` style options``:
'--output dir' = Specify a different output directory
'--filters disabled' = Enable/disable filters

I:
|
\\ append
\\
| all
| the
\\ things
DOC

RSpec.describe 'Global blackbox test' do
  context 'with a complex sample' do
    document = Enolib.parse(input)

    context 'querying different fields' do
      it 'returns the shopping list correctly' do
        expect(document.field('Shopping List').required_string_values).to eq(['Apples', 'Oranges'])
      end

      it 'returns the date correctly' do
        expect(document.field('Date').required_string_value).to eq('1st of November in the year 2017')
      end

      it 'returns the "I" correctly' do
        body = document.section('Body')
        limbs = body.section('Limbs')
        left_arm = limbs.section('Left Arm')

        expect(left_arm.field('I').required_string_value).to eq('append allthe things')
      end
    end
  end
end
