# frozen_string_literal: true

# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

RSpec.describe 'Querying an existing, single-line, required string comment from the document' do
  it 'produces the expected result' do
    input = "> comment\n" \
            "\n" \
            'field: value'
    
    output = Enolib.parse(input).required_string_comment
    
    expected = 'comment'
    
    expect(output).to eq(expected)
  end
end