# frozen_string_literal: true

# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

RSpec.describe 'Querying an existing required string value from a field' do
  it 'produces the expected result' do
    input = 'field: value'
    
    output = Enolib.parse(input).field('field').required_string_value
    
    expected = 'value'
    
    expect(output).to eq(expected)
  end
end

RSpec.describe 'Querying an existing optional string value from a field' do
  it 'produces the expected result' do
    input = 'field: value'
    
    output = Enolib.parse(input).field('field').optional_string_value
    
    expected = 'value'
    
    expect(output).to eq(expected)
  end
end

RSpec.describe 'Querying a missing optional string value from a field' do
  it 'produces the expected result' do
    input = 'field:'
    
    output = Enolib.parse(input).field('field').optional_string_value
    
    expect(output).to be_nil
  end
end