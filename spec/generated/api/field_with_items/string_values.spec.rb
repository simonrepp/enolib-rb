# frozen_string_literal: true

# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

RSpec.describe 'Querying existing required string values from a field with items' do
  it 'produces the expected result' do
    input = "field:\n" \
            "- item\n" \
            '- item'
    
    output = Enolib.parse(input).field('field').required_string_values
    
    expect(output).to eq(['item', 'item'])
  end
end

RSpec.describe 'Querying existing optional string values from a field with items' do
  it 'produces the expected result' do
    input = "field:\n" \
            "- item\n" \
            '- item'
    
    output = Enolib.parse(input).field('field').optional_string_values
    
    expect(output).to eq(['item', 'item'])
  end
end

RSpec.describe 'Querying missing optional string values from a field with items' do
  it 'produces the expected result' do
    input = "field:\n" \
            "-\n" \
            '-'
    
    output = Enolib.parse(input).field('field').optional_string_values
    
    expect(output).to eq([nil, nil])
  end
end