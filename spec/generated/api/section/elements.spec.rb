# frozen_string_literal: true

# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

RSpec.describe 'Querying all elements from a section' do
  it 'produces the expected result' do
    input = "# section\n" \
            "one: value\n" \
            'two: value'
    
    output = Enolib.parse(input).section('section').elements.map(&:string_key)
    
    expect(output).to eq(['one', 'two'])
  end
end

RSpec.describe 'Querying elements from a section by key' do
  it 'produces the expected result' do
    input = "# section\n" \
            "field: value\n" \
            "other: one\n" \
            'other: two'
    
    output = Enolib.parse(input).section('section').elements('other').map(&:required_string_value)
    
    expect(output).to eq(['one', 'two'])
  end
end