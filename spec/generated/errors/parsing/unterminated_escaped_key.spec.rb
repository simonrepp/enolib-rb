# frozen_string_literal: true

# THIS FILE IS AUTO-GENERATED (Please submit permanent changes to https://codeberg.org/simonrepp/enolib-common)

RSpec.describe 'A single field with an unterminated escaped key' do
  it 'raises the expected ParseError' do
    input = '`field: value'

    begin
      Enolib.parse(input)
    rescue Enolib::ParseError => error
      text = 'The key escape sequence in line 1 is not terminated before the end of the line.'
      
      expect(error.text).to eq(text)
      
      snippet = "   Line | Content\n" \
                ' >    1 | `field: value'
      
      expect(error.snippet).to eq(snippet)
      
      expect(error.selection[:from][:line]).to eq(0)
      expect(error.selection[:from][:column]).to eq(1)
      expect(error.selection[:to][:line]).to eq(0)
      expect(error.selection[:to][:column]).to eq(13)
    end
  end
end