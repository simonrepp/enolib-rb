# frozen_string_literal: true

require 'deep-cover'

require 'enolib'
require 'fileutils'
require 'rspec/cheki'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.expose_dsl_globally = false
  config.pattern = '**/*.spec.rb'
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.warnings = true
end
