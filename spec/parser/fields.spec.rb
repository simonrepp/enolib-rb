# frozen_string_literal: true

input = <<~DOC.strip
key:value
key: value
key: more value
    key    :    value
DOC

RSpec.describe 'Fields' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end
