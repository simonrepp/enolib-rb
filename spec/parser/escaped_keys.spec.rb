# frozen_string_literal: true

input = <<~DOC.strip
`key`:
``k`ey``:
```ke``y```    :
    `` `key` ``    :
DOC

RSpec.describe 'Escaped keys' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end