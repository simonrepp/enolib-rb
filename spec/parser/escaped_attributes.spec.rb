# frozen_string_literal: true

input = <<~DOC.strip
field:
`key`=value
``k`ey`` = value
```ke``y```     = more value
    `` `key` ``     =
DOC

RSpec.describe 'Escaped attributes' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end
