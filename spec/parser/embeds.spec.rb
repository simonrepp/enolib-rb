# frozen_string_literal: true

input = <<~DOC.strip
-- key
value
-- key
    --    key
    more value
  --  key
-- key
-- key
-- --
--
-- --
DOC

RSpec.describe 'Embeds' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end