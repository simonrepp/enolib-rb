# frozen_string_literal: true

input = "\n" \
        " \n" \
        "  \n" \
        "   \n" \
        "\n" \
        " \n" \
        "  \n" \
        "   \n"

RSpec.describe 'Empty lines' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end