# frozen_string_literal: true

input = <<~DOC.strip
field_with_items:
-
- value
-    value
    - more value
    -    value
    -
DOC

RSpec.describe 'Items' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end
