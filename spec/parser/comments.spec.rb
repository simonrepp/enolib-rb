# frozen_string_literal: true

input = <<~DOC.strip
> note
> more notes
    >    note
    >
DOC

RSpec.describe 'Comments' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end