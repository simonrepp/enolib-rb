# frozen_string_literal: true

input = <<~DOC.strip
# key
## long key
    ### key
    ####    key
DOC

RSpec.describe 'Sections' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end