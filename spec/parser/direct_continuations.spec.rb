# frozen_string_literal: true

input = <<~DOC.strip
field:
|
| value
|    value
    | more value
    |    value
    |
DOC

RSpec.describe 'Direct continuations' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end
