# frozen_string_literal: true

input = <<~DOC.strip
field: value
\\ spaced continuation
| direct continuation

field_with_attribute:
attribute = value
\\ spaced continuation
| direct continuation

field_with_item:
- value
\\ spaced continuation
| direct continuation

field:
\\ spaced continuation
| direct continuation

field_with_attribute:
attribute =
\\ spaced continuation
| direct continuation

field_with_item:
-
\\ spaced continuation
| direct continuation
DOC

RSpec.describe 'Continuations' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end
