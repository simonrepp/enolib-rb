# frozen_string_literal: true

input = <<~DOC.strip
field:
key=value
key = value
key = more value
    more key    =    value
DOC

RSpec.describe 'Attributes' do
  it 'parses as expected' do
    expect(Enolib.parse(input).snippet).to match_snapshot
  end
end