# frozen_string_literal: true

require_relative './space'

grammar = Enolib.const_get('Grammar')

SCENARIOS = [
  # DIRECT_CONTINUATION_SCENARIOS
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '|'
    },
    syntax: '|',
    variants: space('|')
  },
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '|',
      grammar::CONTINUATION_VALUE_INDEX => 'Value'
    },
    syntax: '| Value',
    variants: space('|', 'Value')
  },
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '|',
      grammar::CONTINUATION_VALUE_INDEX => '|'
    },
    syntax: '| |',
    variants: space('|', '|')
  },

  # SPACED_CONTINUATION_SCENARIOS
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '\\'
    },
    syntax: '\\',
    variants: space('\\')
  },
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '\\',
      grammar::CONTINUATION_VALUE_INDEX => 'Value'
    },
    syntax: '\\ Value',
    variants: space('\\', 'Value')
  },
  {
    captures: {
      grammar::CONTINUATION_OPERATOR_INDEX => '\\',
      grammar::CONTINUATION_VALUE_INDEX => '\\'
    },
    syntax: '\\ \\',
    variants: space('\\', '\\')
  },

  # EMBED_SCENARIOS
  {
    captures: {
      grammar::EMBED_OPERATOR_INDEX => '--',
      grammar::EMBED_KEY_INDEX => 'Key'
    },
    syntax: '-- Key',
    variants: space('--', 'Key')
  },
  {
    captures: {
      grammar::EMBED_OPERATOR_INDEX => '--',
      grammar::EMBED_KEY_INDEX => '--'
    },
    syntax: '-- --',
    variants: space('--', ' ', '--')
  },
  {
    captures: {
      grammar::EMBED_OPERATOR_INDEX => '---',
      grammar::EMBED_KEY_INDEX => 'The Key'
    },
    syntax: '--- The Key',
    variants: space('---', 'The Key')
  },
  {
    captures: {
      grammar::EMBED_OPERATOR_INDEX => '---',
      grammar::EMBED_KEY_INDEX => '---'
    },
    syntax: '--- ---',
    variants: space('---', ' ', '---')
  },

  # COMMENT_SCENARIOS
  {
    captures: {
      grammar::COMMENT_OPERATOR_INDEX => '>',
      grammar::COMMENT_VALUE_INDEX => 'Comment Value'
    },
    syntax: '>Comment Value',
    variants: ['>Comment Value', ' >Comment Value', '   >Comment Value']
  },
  {
    captures: {
      grammar::COMMENT_OPERATOR_INDEX => '>',
      grammar::COMMENT_VALUE_INDEX => 'Comment Value'
    },
    syntax: '> Comment Value',
    variants: ['> Comment Value', ' > Comment Value', '   > Comment Value']
  },
  {
    captures: {
      grammar::COMMENT_OPERATOR_INDEX => '>',
      grammar::COMMENT_VALUE_INDEX => 'Comment Value'
    },
    syntax: '> Comment Value ',
    variants: ['> Comment Value ', ' > Comment Value ', '   > Comment Value ']
  },
  {
    captures: {
      grammar::COMMENT_OPERATOR_INDEX => '>',
      grammar::COMMENT_VALUE_INDEX => 'Comment Value'
    },
    syntax: '>   Comment Value   ',
    variants: ['>   Comment Value   ', ' >   Comment Value   ', '   >   Comment Value   ']
  },

  # ATTRIBUTE_SCENARIOS
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => 'Value'
    },
    syntax: 'Key = Value',
    variants: space('Key', '=', 'Value')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'The Key',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => 'The Value'
    },
    syntax: 'The Key = The Value',
    variants: space('The Key', '=', 'The Value')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => '='
    },
    syntax: 'Key = =',
    variants: space('Key', '=', ' ', '=')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => ':'
    },
    syntax: 'Key = :',
    variants: space('Key', '=', ' ', ':')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '`',
      grammar::KEY_ESCAPED_INDEX => '=:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '`',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => '`=:`'
    },
    syntax: '`=:` = `=:`',
    variants: space('`', '=:', '`', '=', '`=:`')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '```',
      grammar::KEY_ESCAPED_INDEX => '`=``:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '```',
      grammar::ATTRIBUTE_OPERATOR_INDEX => '=',
      grammar::ATTRIBUTE_VALUE_INDEX => '`=:`'
    },
    syntax: '``` `=``:``` = `=:`',
    variants: space('```', ' ', '`=``:', '```', '=', '`=:`')
  },

  # EMPTY_LINE_SCENARIOS
  {
    captures: {
      grammar::EMPTY_LINE_INDEX => ''
    },
    syntax: '',
    variants: space('')
  },

  # FIELD_SCENARIOS
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::FIELD_OPERATOR_INDEX => ':',
      grammar::FIELD_VALUE_INDEX => 'Value'
    },
    syntax: 'Key: Value',
    variants: space('Key', ':', 'Value')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'The Key',
      grammar::FIELD_OPERATOR_INDEX => ':',
      grammar::FIELD_VALUE_INDEX => 'The Value'
    },
    syntax: 'The Key: The Value',
    variants: space('The Key', ':', 'The Value')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::FIELD_OPERATOR_INDEX => ':',
      grammar::FIELD_VALUE_INDEX => ':'
    },
    syntax: 'Key: :',
    variants: space('Key', ':', ' ', ':')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '`',
      grammar::KEY_ESCAPED_INDEX => '=:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '`',
      grammar::FIELD_OPERATOR_INDEX => ':',
      grammar::FIELD_VALUE_INDEX => '`=:`'
    },
    syntax: '`=:` : `=:`',
    variants: space('`', '=:', '`', ':', '`=:`')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '```',
      grammar::KEY_ESCAPED_INDEX => '`=``:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '```',
      grammar::FIELD_OPERATOR_INDEX => ':',
      grammar::FIELD_VALUE_INDEX => '`=:`'
    },
    syntax: '``` `=``:``` : `=:`',
    variants: space('```', ' ', '`=``:', '```', ':', '`=:`')
  },

  # INVALID_SCENARIOS
  {
    syntax: '#',
    variants: space('#')
  },
  {
    syntax: '--',
    variants: space('--')
  },
  {
    syntax: ':',
    variants: space(':')
  },
  {
    syntax: ': Invalid',
    variants: space(':', 'Invalid')
  },
  {
    syntax: '=',
    variants: space('=')
  },
  {
    syntax: '= Invalid',
    variants: space('=', 'Invalid')
  },
  {
    syntax: '---',
    variants: space('---')
  },
  {
    syntax: ": Invalid\nValid:",
    variants: space(':', 'Invalid', "\n", 'Valid', ':')
  },
  {
    syntax: ": Invalid\nValid:Valid",
    variants: space(':', 'Invalid', "\n", 'Valid', ':', 'Valid')
  },

  # ITEM_SCENARIOS
  {
    captures: {
      grammar::ITEM_OPERATOR_INDEX => '-'
    },
    syntax: '-',
    variants: space('-')
  },
  {
    captures: {
      grammar::ITEM_OPERATOR_INDEX => '-',
      grammar::ITEM_VALUE_INDEX => 'Item'
    },
    syntax: '- Item',
    variants: space('-', 'Item')
  },
  {
    captures: {
      grammar::ITEM_OPERATOR_INDEX => '-',
      grammar::ITEM_VALUE_INDEX => 'The Item'
    },
    syntax: '- The Item',
    variants: space('-', 'The Item')
  },
  {
    captures: {
      grammar::ITEM_OPERATOR_INDEX => '-',
      grammar::ITEM_VALUE_INDEX => '-'
    },
    syntax: '- -',
    variants: space('-', ' ', '-')
  },

  # FIELD_OR_FIELDSET_OR_LIST_SCENARIOS
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'Key',
      grammar::FIELD_OPERATOR_INDEX => ':'
    },
    syntax: 'Key:',
    variants: space('Key', ':')
  },
  {
    captures: {
      grammar::KEY_UNESCAPED_INDEX => 'The Key',
      grammar::FIELD_OPERATOR_INDEX => ':'
    },
    syntax: 'The Key:',
    variants: space('The Key', ':')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '`',
      grammar::KEY_ESCAPED_INDEX => '=:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '`',
      grammar::FIELD_OPERATOR_INDEX => ':'
    },
    syntax: '`=:`:',
    variants: space('`', '=:', '`', ':')
  },
  {
    captures: {
      grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX => '```',
      grammar::KEY_ESCAPED_INDEX => '`=``:',
      grammar::KEY_ESCAPE_END_OPERATOR_INDEX => '```',
      grammar::FIELD_OPERATOR_INDEX => ':'
    },
    syntax: '``` `=``:```:',
    variants: space('```', ' ', '`=``:', '```', ':')
  },

  # SECTION_SCENARIOS
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '#',
      grammar::SECTION_KEY_INDEX => 'Key'
    },
    syntax: '# Key',
    variants: space('#', 'Key')
  },
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '##',
      grammar::SECTION_KEY_INDEX => 'The Key'
    },
    syntax: '## The Key',
    variants: space('##', 'The Key')
  },
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '#',
      grammar::SECTION_KEY_INDEX => '# Other Key'
    },
    syntax: '# # Other Key',
    variants: space('#', ' ', '# Other Key')
  },
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '###',
      grammar::SECTION_KEY_INDEX => '## ###'
    },
    syntax: '### ## ###',
    variants: space('###', ' ', '## ###')
  },
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '#',
      grammar::SECTION_KEY_INDEX => '`=:` `=:`'
    },
    syntax: '# `=:` `=:`',
    variants: space('#', '`=:` `=:`')
  },
  {
    captures: {
      grammar::SECTION_OPERATOR_INDEX => '#',
      grammar::SECTION_KEY_INDEX => '``` `=``:```  ``` `=``:```'
    },
    syntax: '# ``` `=``:```  ``` `=``:```',
    variants: space('#', '``` `=``:```  ``` `=``:```')
  }
].freeze
