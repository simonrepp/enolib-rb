# frozen_string_literal: true

module Enolib
  class HtmlReporter < Reporter
    HTML_ESCAPE = {
      '&' => '&amp;',
      '<' => '&lt;',
      '>' => '&gt;',
      '"' => '&quot;',
      "'" => '&#39;',
      '/' => '&#x2F;'
    }.freeze
    
    private_constant :HTML_ESCAPE

    private

    def escape(string)
      string.gsub(/[&<>"'\/]/) { |c| HTML_ESCAPE[c] }
    end

    def markup(gutter, content, tag_class = '')
      "<div class=\"eno-report-line #{tag_class}\">" \
      "<div class=\"eno-report-gutter\">#{gutter.rjust(10)}</div>" \
      "<div class=\"eno-report-content\">#{escape(content)}</div>" \
      '</div>'
    end

    def print
      columns_header = markup(@context.messages::GUTTER_HEADER, @context.messages::CONTENT_HEADER)
      snippet = @snippet.each_with_index.map { |tag, line| print_line(line, tag) if tag }.compact.join("\n")

      if @context.source
        return "<div><div>#{@context.source}</div><pre class=\"eno-report\">#{columns_header}#{snippet}</pre></div>"
      end

      "<pre class=\"eno-report\">#{columns_header}#{snippet}</pre>"
    end
    
    def print_line(line, tag)
      return markup('...', '...') if tag == :omission

      number = (line + HUMAN_INDEXING).to_s
      instruction = @index[line]

      content = ''
      if instruction
        content = @context.input[instruction[:ranges][:line][RANGE_BEGIN]..instruction[:ranges][:line][RANGE_END]]
      end

      tag_class =
        case tag
        when :emphasize
          'eno-report-line-emphasized'
        when :indicate
          'eno-report-line-indicated'
        when :question
          'eno-report-line-questioned'
        else
          ''
        end

      markup(number, content, tag_class)
    end
  end
end
