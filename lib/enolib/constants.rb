# frozen_string_literal: true

module Enolib
  HUMAN_INDEXING = 1
  PRETTY_TYPES = {
    attribute: 'attribute',
    document: 'document',
    embed_begin: 'embed',
    field: 'field',
    flag: 'flag',
    item: 'item',
    section: 'section'
  }.freeze
  RANGE_BEGIN = 0
  RANGE_END = 1
  
  private_constant :HUMAN_INDEXING,
                   :PRETTY_TYPES,
                   :RANGE_BEGIN,
                   :RANGE_END
end