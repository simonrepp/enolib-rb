# frozen_string_literal: true

module Enolib
  class Parser
    # = value
    # : value
    ATTRIBUTE_OR_FIELD_WITHOUT_KEY = /^\s*([:=]).*$/.freeze
    
    # --
    # #
    EMBED_OR_SECTION_WITHOUT_KEY = /^\s*(--+|#+).*$/.freeze
    
    # ` `
    ESCAPE_WITHOUT_KEY = /^\s*(`+)(?!`)(\s+)(\1).*$/.freeze
    
    # `key` value
    INVALID_AFTER_ESCAPE = /^\s*(`+)(?!`)(?:(?!\1).)+\1\s*([^=:].*?)\s*$/.freeze
    
    # `key
    UNTERMINATED_ESCAPED_KEY = /^\s*(`+)(?!`)(.*)$/.freeze
    
    def initialize(context)
      @context = context
      @depth = 0
      @index = 0
      @line = 0
    end

    def run
      if @context.input.empty?
        @context.line_count = 1
        return
      end

      comments = nil
      last_continuable_element = nil
      last_field = nil
      last_section = @context.document

      while @index < @context.input.length
        match = Grammar::REGEX.match(@context.input, @index)

        unless match && match.begin(0) == @index
          instruction = parse_after_error
          invalid_line(instruction)
        end

        instruction = {
          index: @index,
          line: @line,
          ranges: {
            line: match.offset(0)
          }
        }

        embed = false

        if match[Grammar::EMPTY_LINE_INDEX]

          if comments
            if comments.first[:line] == 0
              @context.document[:comments] = comments
            else
              @context.meta.concat(comments)
            end
            comments = nil
          end

        elsif match[Grammar::FIELD_OPERATOR_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:key] = match[Grammar::KEY_UNESCAPED_INDEX]
          instruction[:type] = :field

          if instruction[:key]
            instruction[:ranges][:field_operator] = match.offset(Grammar::FIELD_OPERATOR_INDEX)
            instruction[:ranges][:key] = match.offset(Grammar::KEY_UNESCAPED_INDEX)
          else
            instruction[:key] = match[Grammar::KEY_ESCAPED_INDEX]
            instruction[:ranges][:field_operator] = match.offset(Grammar::FIELD_OPERATOR_INDEX)
            instruction[:ranges][:escape_begin_operator] = match.offset(Grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX)
            instruction[:ranges][:escape_end_operator] = match.offset(Grammar::KEY_ESCAPE_END_OPERATOR_INDEX)
            instruction[:ranges][:key] = match.offset(Grammar::KEY_ESCAPED_INDEX)
          end

          value = match[Grammar::FIELD_VALUE_INDEX]

          if value
            instruction[:ranges][:value] = match.offset(Grammar::FIELD_VALUE_INDEX)
            instruction[:value] = value
          end

          instruction[:parent] = last_section
          last_section[:elements].push(instruction)
          last_continuable_element = instruction
          last_field = instruction

        elsif match[Grammar::ITEM_OPERATOR_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:ranges][:item_operator] = match.offset(Grammar::ITEM_OPERATOR_INDEX)
          instruction[:type] = :item

          value = match[Grammar::ITEM_VALUE_INDEX]

          if value
            instruction[:ranges][:value] = match.offset(Grammar::ITEM_VALUE_INDEX)
            instruction[:value] = value
          end

          if !last_field
            parse_after_error(instruction)
            instruction_outside_field(instruction, 'item')
          elsif last_field.has_key?(:items)
            last_field[:items].push(instruction)
          elsif last_field.has_key?(:attributes) ||
                last_field.has_key?(:continuations) ||
                last_field.has_key?(:value)
            parse_after_error(instruction)
            mixed_field_content(last_field, instruction)
          else
            last_field[:items] = [instruction]
          end

          instruction[:parent] = last_field
          last_continuable_element = instruction

        elsif match[Grammar::ATTRIBUTE_OPERATOR_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:type] = :attribute

          instruction[:key] = match[Grammar::KEY_UNESCAPED_INDEX]

          if instruction[:key]
            instruction[:ranges][:key] = match.offset(Grammar::KEY_UNESCAPED_INDEX)
            instruction[:ranges][:attribute_operator] = match.offset(Grammar::ATTRIBUTE_OPERATOR_INDEX)
          else
            instruction[:key] = match[Grammar::KEY_ESCAPED_INDEX]
            instruction[:ranges][:attribute_operator] = match.offset(Grammar::ATTRIBUTE_OPERATOR_INDEX)
            instruction[:ranges][:escape_begin_operator] = match.offset(Grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX)
            instruction[:ranges][:escape_end_operator] = match.offset(Grammar::KEY_ESCAPE_END_OPERATOR_INDEX)
            instruction[:ranges][:key] = match.offset(Grammar::KEY_ESCAPED_INDEX)
          end

          value = match[Grammar::ATTRIBUTE_VALUE_INDEX]

          if value
            instruction[:ranges][:value] = match.offset(Grammar::ATTRIBUTE_VALUE_INDEX)
            instruction[:value] = value
          end
          
          if !last_field
            parse_after_error(instruction)
            instruction_outside_field(instruction, 'attribute')
          elsif last_field.has_key?(:attributes)
            last_field[:attributes].push(instruction)
          elsif last_field.has_key?(:continuations) ||
                last_field.has_key?(:items) ||
                last_field.has_key?(:value)
            parse_after_error(instruction)
            mixed_field_content(last_field, instruction)
          else
            last_field[:attributes] = [instruction]
          end

          instruction[:parent] = last_field
          last_continuable_element = instruction

        elsif match[Grammar::CONTINUATION_OPERATOR_INDEX]

          if match[Grammar::CONTINUATION_OPERATOR_INDEX] == '\\'
            instruction[:spaced] = true
            instruction[:ranges][:spaced_continuation_operator] = match.offset(Grammar::CONTINUATION_OPERATOR_INDEX)
          else
            instruction[:ranges][:direct_continuation_operator] = match.offset(Grammar::CONTINUATION_OPERATOR_INDEX)
          end

          instruction[:type] = :continuation

          value = match[Grammar::CONTINUATION_VALUE_INDEX]

          if value
            instruction[:ranges][:value] = match.offset(Grammar::CONTINUATION_VALUE_INDEX)
            instruction[:value] = value
          end

          unless last_continuable_element
            parse_after_error(instruction)
            instruction_outside_field(instruction, 'continuation')
          end

          if last_continuable_element.has_key?(:continuations)
            last_continuable_element[:continuations].push(instruction)
          else
            last_continuable_element[:continuations] = [instruction]
          end

          if comments
            @context.meta.concat(comments)
            comments = nil
          end

        elsif match[Grammar::SECTION_OPERATOR_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:elements] = []
          instruction[:ranges][:section_operator] = match.offset(Grammar::SECTION_OPERATOR_INDEX)
          instruction[:type] = :section

          instruction[:key] = match[Grammar::SECTION_KEY_INDEX]
          instruction[:ranges][:key] = match.offset(Grammar::SECTION_KEY_INDEX)

          new_depth = instruction[:ranges][:section_operator][1] - instruction[:ranges][:section_operator][0]

          if new_depth == @depth + 1
            instruction[:parent] = last_section
            @depth = new_depth
          elsif new_depth == @depth
            instruction[:parent] = last_section[:parent]
          elsif new_depth < @depth
            while new_depth < @depth
              last_section = last_section[:parent]
              @depth -= 1
            end

            instruction[:parent] = last_section[:parent]
          else
            parse_after_error(instruction)
            section_level_skip(instruction, last_section)
          end

          instruction[:parent][:elements].push(instruction)

          last_section = instruction
          last_continuable_element = nil
          last_field = nil

        elsif match[Grammar::EMBED_OPERATOR_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          operator = match[Grammar::EMBED_OPERATOR_INDEX]
          key = match[Grammar::EMBED_KEY_INDEX]

          instruction[:key] = key
          instruction[:parent] = last_section
          instruction[:ranges][:embed_operator] = match.offset(Grammar::EMBED_OPERATOR_INDEX)
          instruction[:ranges][:key] = match.offset(Grammar::EMBED_KEY_INDEX)
          instruction[:type] = :embed_begin

          @index = match.end(0)

          last_section[:elements].push(instruction)
          last_continuable_element = nil
          begin_instruction = instruction

          terminator_regex = /\n[^\S\n]*(#{operator})(?!-)[^\S\n]*(#{Regexp.escape(key)})[^\S\n]*(?=\n|$)/
          terminator_match = terminator_regex.match(@context.input, @index)

          @index += 1  # move past current char (\n) into next line
          @line += 1

          unless terminator_match
            parse_after_error
            unterminated_embed(instruction)
          end

          end_of_embed_index = terminator_match.begin(0)

          if end_of_embed_index != @index - 1
            instruction[:lines] = []

            loop do
              end_of_line_index = @context.input.index("\n", @index)

              if !end_of_line_index || end_of_line_index >= end_of_embed_index
                begin_instruction[:lines].push(
                  line: @line,
                  ranges: {
                    line: [@index, end_of_embed_index],
                    value: [@index, end_of_embed_index]
                  },
                  type: :embed_value
                )

                @index = end_of_embed_index + 1
                @line += 1

                break
              else
                begin_instruction[:lines].push(
                  line: @line,
                  ranges: {
                    line: [@index, end_of_line_index],
                    value: [@index, end_of_line_index]
                  },
                  type: :embed_value
                )

                @index = end_of_line_index + 1
                @line += 1
              end
            end
          end

          instruction = {
            length: terminator_match.end(0),
            line: @line,
            ranges: {
              key: terminator_match.offset(2),
              line: [@index, terminator_match.end(0)],
              embed_operator: terminator_match.offset(1)
            },
            type: :embed_end
          }

          begin_instruction[:end] = instruction
          last_field = nil

          @index = terminator_match.end(0) + 1
          @line += 1

          embed = true

        elsif match[Grammar::COMMENT_OPERATOR_INDEX]

          if comments
            comments.push(instruction)
          else
            comments = [instruction]
          end

          instruction[:ranges][:comment_operator] = match.offset(Grammar::COMMENT_OPERATOR_INDEX)
          instruction[:type] = :comment

          comment = match[Grammar::COMMENT_VALUE_INDEX]

          if comment
            instruction[:comment] = comment
            instruction[:ranges][:comment] = match.offset(Grammar::COMMENT_VALUE_INDEX)
          end

        elsif match[Grammar::KEY_UNESCAPED_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:key] = match[Grammar::KEY_UNESCAPED_INDEX]
          instruction[:ranges][:key] = match.offset(Grammar::KEY_UNESCAPED_INDEX)
          instruction[:type] = :flag

          instruction[:parent] = last_section
          last_section[:elements].push(instruction)
          last_continuable_element = nil
          last_field = instruction

        elsif match[Grammar::KEY_ESCAPED_INDEX]

          if comments
            instruction[:comments] = comments
            comments = nil
          end

          instruction[:key] = match[Grammar::KEY_ESCAPED_INDEX]
          instruction[:ranges][:escape_begin_operator] = match.offset(Grammar::KEY_ESCAPE_BEGIN_OPERATOR_INDEX)
          instruction[:ranges][:escape_end_operator] = match.offset(Grammar::KEY_ESCAPE_END_OPERATOR_INDEX)
          instruction[:ranges][:key] = match.offset(Grammar::KEY_ESCAPED_INDEX)
          instruction[:type] = :flag

          instruction[:parent] = last_section
          last_section[:elements].push(instruction)
          last_continuable_element = nil
          last_field = instruction

        end

        unless embed
          @index = match.end(0) + 1
          @line += 1
        end
      end

      @context.line_count = @context.input[-1] == "\n" ? @line + 1 : @line
      @context.meta.concat(comments) if comments
    end

    private
    
    def attribute_or_field_without_key(instruction, match)
      operator = match[1]
      operator_column = match.begin(1)
      message = operator == '=' ? 'attribute_without_key' : 'field_without_key'
      raise ParseError.new(
        @context.messages.send(message, instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        {
          from: ErrorSelection.cursor(instruction, :line, RANGE_BEGIN),
          to: {
            column: operator_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + operator_column,
            line: instruction[:line]
          }
        }
      )
    end
    
    def embed_or_section_without_key(instruction, match)
      operator = match[1]
      key_column = match.end(1)
      message = operator.start_with?('-') ? 'embed_without_key' : 'section_without_key'
      raise ParseError.new(
        @context.messages.send(message, instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        {
          from: {
            column: key_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + key_column,
            line: instruction[:line]
          },
          to: ErrorSelection.cursor(instruction, :line, RANGE_END)
        }
      )
    end
    
    def escape_without_key(instruction, match)
      gap_begin_column = match.end(1)
      gap_end_column = match.begin(3)
      raise ParseError.new(
        @context.messages.escape_without_key(instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        {
          from: {
            column: gap_begin_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + gap_begin_column,
            line: instruction[:line]
          },
          to: {
            column: gap_end_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + gap_end_column,
            line: instruction[:line]
          }
        }
      )
    end

    def invalid_line(instruction)
      line = @context.input[instruction[:ranges][:line][RANGE_BEGIN]..instruction[:ranges][:line][RANGE_END]]

      match = ATTRIBUTE_OR_FIELD_WITHOUT_KEY.match(line)
      attribute_or_field_without_key(instruction, match) if match
      
      match = EMBED_OR_SECTION_WITHOUT_KEY.match(line)
      embed_or_section_without_key(instruction, match) if match
      
      match = ESCAPE_WITHOUT_KEY.match(line)
      escape_without_key(instruction, match) if match
      
      match = INVALID_AFTER_ESCAPE.match(line)
      invalid_after_escape(instruction, match) if match
      
      match = UNTERMINATED_ESCAPED_KEY.match(line)
      unterminated_escaped_key(instruction, match) if match
    end

    def instruction_outside_field(instruction, type)
      raise ParseError.new(
        @context.messages.send(type + '_outside_field', instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        ErrorSelection.select_line(instruction)
      )
    end
    
    def invalid_after_escape(instruction, match)
      invalid_begin_column = match.begin(2)
      invalid_end_column = match.end(2)
      raise ParseError.new(
        @context.messages.invalid_after_escape(instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        {
          from: {
            column: invalid_begin_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + invalid_begin_column,
            line: instruction[:line]
          },
          to: {
            column: invalid_end_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + invalid_end_column,
            line: instruction[:line]
          }
        }
      )
    end
    
    def mixed_field_content(field, conflicting)
      raise ParseError.new(
        @context.messages.mixed_field_content(field[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).indicate_element(field).report_line(conflicting).snippet,
        ErrorSelection.select_line(conflicting)
      )
    end
    
    def parse_after_error(error_instruction = nil)
      if error_instruction
        @context.meta.push(error_instruction)
        @index = error_instruction[:ranges][:line][RANGE_END]
        @line += 1
      end

      while @index < @context.input.length
        end_of_line_index = @context.input.index("\n", @index) || @context.input.length

        instruction = {
          line: @line,
          ranges: { line: [@index, end_of_line_index] },
          type: :unparsed
        }

        error_instruction ||= instruction

        @context.meta.push(instruction)
        @index = end_of_line_index + 1
        @line += 1
      end

      @context.line_count = @context.input[-1] == "\n" ? @line + 1 : @line

      error_instruction
    end

    def section_level_skip(section, super_section)
      reporter = @context.reporter.new(@context).report_line(section)

      reporter.indicate_line(super_section) if super_section[:type] != :document

      raise ParseError.new(
        @context.messages.section_level_skip(section[:line] + HUMAN_INDEXING),
        reporter.snippet,
        ErrorSelection.select_line(section)
      )
    end

    def unterminated_escaped_key(instruction, match)
      selection_column = match.end(1)
      raise ParseError.new(
        @context.messages.unterminated_escaped_key(instruction[:line] + HUMAN_INDEXING),
        @context.reporter.new(@context).report_line(instruction).snippet,
        {
          from: {
            column: selection_column,
            index: instruction[:ranges][:line][RANGE_BEGIN] + selection_column,
            line: instruction[:line]
          },
          to: ErrorSelection.cursor(instruction, :line, RANGE_END)
        }
      )
    end

    def unterminated_embed(field)
      reporter = @context.reporter.new(@context).report_element(field)

      @context.meta.each do |instruction|
        reporter.indicate_line(instruction) if instruction[:line] > field[:line]
      end

      raise ParseError.new(
        @context.messages.unterminated_embed(field[:key], field[:line] + HUMAN_INDEXING),
        reporter.snippet,
        ErrorSelection.select_line(field)
      )
    end
  end
  
  private_constant :Parser
end
