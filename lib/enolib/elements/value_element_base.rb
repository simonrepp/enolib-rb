# frozen_string_literal: true

module Enolib
  class ValueElementBase < ElementBase
    def optional_string_value
      _value(required: false)
    end

    def optional_value(loader = nil, &block)
      loader = Proc.new(&block) if block_given?

      unless loader
        raise ArgumentError, 'A loader function must be provided'
      end

      _value(loader, required: false)
    end

    def required_string_value
      _value(required: true)
    end

    def required_value(loader = nil, &block)
      loader = Proc.new(&block) if block_given?

      unless loader
        raise ArgumentError, 'A loader function must be provided'
      end

      _value(loader, required: true)
    end

    def value_error(message = nil)
      if block_given?
        message = yield(@context.value(@instruction))
      elsif message.is_a?(Proc)
        message = message.call(@context.value(@instruction))
      end

      unless message
        raise ArgumentError, 'A message or message function must be provided'
      end

      _value_error(message, @instruction)
    end

    private
    
    def _missing_value_error(element)
      selection = {}

      if element[:type] == :field ||
         element[:type] == :embed_begin
        message = @context.messages.missing_field_value(element[:key])

        selection[:from] =
          if element[:ranges].has_key?(:field_operator)
            ErrorSelection.cursor(element, :field_operator, RANGE_END)
          else
            ErrorSelection.cursor(element, :line, RANGE_END)
          end
      elsif element[:type] == :attribute
        message = @context.messages.missing_attribute_value(element[:key])
        selection[:from] = ErrorSelection.cursor(element, :attribute_operator, RANGE_END)
      elsif element[:type] == :item
        message = @context.messages.missing_item_value(element[:parent][:key])
        selection[:from] = ErrorSelection.cursor(element, :item_operator, RANGE_END)
      end

      snippet = @context.reporter.new(@context).report_element(element).snippet

      selection[:to] =
        if element[:type] == :field && element.has_key?(:continuations)
          ErrorSelection.cursor(element[:continuations].last, :line, RANGE_END)
        else
          ErrorSelection.cursor(element, :line, RANGE_END)
        end

      ValidationError.new(message, snippet, selection)
    end

    def print_value
      value = @context.value(@instruction)

      return 'nil' unless value

      value = "#{value[0..10]}..." if value.length > 14

      value.gsub("\n", '\n')
    end

    def _value(loader = nil, required:)
      @touched = true

      value = @context.value(@instruction)

      if value
        return value unless loader

        begin
          loader.call(value)
        rescue => message
          raise _value_error(message, @instruction)
        end
      else
        return nil unless required

        raise _missing_value_error(@instruction)
      end
    end
    
    def _value_error(message, element)
      if element[:type] == :embed_begin
        if element.has_key?(:lines)
          snippet = @context.reporter.new(@context).report_multiline_value(element).snippet
          select = ErrorSelection.selection(element[:lines][0], :line, RANGE_BEGIN, element[:lines][-1], :line, RANGE_END)
        else
          snippet = @context.reporter.new(@context).report_element(element).snippet
          select = ErrorSelection.selection(element, :line, RANGE_END)
        end
      else
        snippet = @context.reporter.new(@context).report_element(element).snippet
        select = {
          from:
            if element[:ranges].has_key?(:value)
              ErrorSelection.cursor(element, :value, RANGE_BEGIN)
            elsif element[:ranges].has_key?(:field_operator)
              ErrorSelection.cursor(element, :field_operator, RANGE_END)
            elsif element[:ranges].has_key?(:attribute_operator)
              ErrorSelection.cursor(element, :attribute_operator, RANGE_END)
            elsif element[:type] == :item
              ErrorSelection.cursor(element, :item_operator, RANGE_END)
            else
              # TODO: Possibly never reached - think through state permutations
              ErrorSelection.cursor(element, :line, RANGE_END)
            end,
          to:
            if element.has_key?(:continuations)
              ErrorSelection.cursor(element[:continuations][-1], :line, RANGE_END)
            elsif element[:ranges].has_key?(:value)
              ErrorSelection.cursor(element, :value, RANGE_END)
            else
              ErrorSelection.cursor(element, :line, RANGE_END)
            end
        }
      end

      ValidationError.new(@context.messages.value_error(message), snippet, select)
    end
  end
  
  private_constant :ValueElementBase
end
